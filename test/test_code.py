import pytest
import mock 
import requests
from project.source import getAPI
from project.source import transform

weather= {
    'coord': {'lon': -0.1257, 'lat': 51.5085},
    'weather': [{'id': 804,
        'main': 'Clouds',
        'description': 'overcast clouds',
        'icon': '04d'}],
    'base': 'stations',
    'main': {'temp': 50,
        'feels_like': 47.8,
        'temp_min': 46.13,
        'temp_max': 52.83,
        'pressure': 1016,
        'humidity': 90},
    'visibility': 10000,
    'wind': {'speed': 5.75, 'deg': 0, 'gust': 17.27},
    'clouds': {'all': 100},
    'dt': 1654065843,
    'sys': {'type': 2,
        'id': 2019646,
        'country': 'GB',
        'sunrise': 1654055356,
        'sunset': 1654114062},
    'timezone': 3600,
    'id': 2643743,
    'name': 'London',
    'cod': 200}


@mock.patch('requests.request')
def test_getAPI (mock_getAPI):
    mock_response=mock.Mock(name="mock_reponse", **{"status_code":200, "json.return_value": weather})
    mock_getAPI.return_value =mock_response
    assert getAPI() == weather
    
@pytest.fixture
def transformcheck():
    return transform(1654055356,52.83)

def test_transformUTC(transformcheck):
    assert transformcheck.transformUTC() == '18:00:52'

def test_temptransform(transformcheck):
    assert transformcheck.temptransform() == 11.600226999999999
    
